
--
-- JobManager  (1.2?)
--   by: thiconZ
--		fixed up by: Xsear
--      for PTS stabilization-1325
--      2015-08-19
--	Manage your jobs

-- Requires
require "math"
require "unicode"
require "table"
require "lib/lib_Slash"
require "lib/lib_Callback2"
require "lib/lib_Debug"
require "lib/lib_MovablePanel"

local MAIN	=	Component.GetFrame("Main")
local TT	=	Component.GetWidget("TitleText")
local JLA   =   nil
local RSA   =   nil
local RCA   =   nil
local JLS   =   nil
local GJS   =   nil
local JPER	=	Component.GetWidget("percent")
local NAME	=	Component.GetWidget("name")
local JID	=	Component.GetWidget("jobid")
local DESC	=	Component.GetWidget("description")

local COLOR_WARN = "eeee00"
local RESTART_DELAY_SECONDS = 1
local DEBUG_ENABLED = false


-- FUNCTIONS
function OnComponentLoad()
	Debug.EnableLogging(DEBUG_ENABLED)
	
	MAIN:Show(false)
	Component.SetInputMode("none")

	MovablePanel.ConfigFrame({
        frame = MAIN,
        MOVABLE_PARENT = Component.GetWidget("MovableParent")
    })

	TT:SetText("Job Manager")
	JLA = Component.CreateWidget('<Button dimensions="dock:fill"/>', Component.GetWidget("JoinLeaderArc"))
	JLA:SetText("Sync With Leader")
	JLA:SetParam("tint", COLOR_WARN)
	JLA:BindEvent("OnMouseDown", JoinSquadLeadersArc)
	RSA = Component.CreateWidget('<Button dimensions="dock:fill"/>', Component.GetWidget("RequestRestartArc"))
	RSA:SetText("Restart Current Job")
	RSA:SetParam("tint", COLOR_WARN)
	RSA:BindEvent("OnMouseDown", RequestRestartArc)
	RCA = Component.CreateWidget('<Button dimensions="dock:fill"/>', Component.GetWidget("RequestCancelArc"))
	RCA:SetText("Abandon Current Job")
	RCA:SetParam("tint", COLOR_WARN)
	RCA:BindEvent("OnMouseDown", RequestCancelArc)
	JLS = Component.CreateWidget('<Button dimensions="dock:fill"/>', Component.GetWidget("LeaderStatus"))
	JLS:SetText("Get Leader Status")
	JLS:BindEvent("OnMouseDown", GetLeaderJobStatus)
	GJS = Component.CreateWidget('<Button dimensions="dock:fill"/>', Component.GetWidget("GetJobStatus"))
	GJS:SetText("Get Job Status")
	GJS:BindEvent("OnMouseDown", GetJobStatus)

	ResetText()

	LIB_SLASH.BindCallback({slash_list="sjm", description="Show Job Manager", func=ShowJobManager})
	LIB_SLASH.BindCallback({slash_list="jm", description="Show Job Manager", func=ShowJobManager})
	LIB_SLASH.BindCallback({slash_list="jobmanager", description="jobmanager", func=ShowJobManager})
	LIB_SLASH.BindCallback({slash_list="hjm", description="Hide Job Manager", func=HideJobManager})
end

function JoinSquadLeadersArc()
	Debug.Log("Requesting Squad Leader Arc Sync")
	RequestCancelArc()
	Game.JoinSquadLeadersArc()
end

function RequestCancelArc()
	if (Player.GetJobStatus() ~= nil) then
		local arc_id = tostring(Player.GetJobStatus().job.arc_id)
		Debug.Log("Requesting Arc Cancel For ID: ", arc_id)
		Game.RequestCancelArc(arc_id)
	end
end

function GetLeaderJobStatus()
	Debug.Log("Requesting Squad Leader Job Status")
	local status = Squad.GetLeaderJobStatus()
	Debug.Table("status", status)
	ResetText()
	if (status ~= nil) then
		UpdateText(status)
	end
end

function GetJobStatus()
	Debug.Log("Requesting Player Job Status")
	local status = Player.GetJobStatus()
	Debug.Table("status", status)
	ResetText()
	if (status ~= nil) then
		UpdateText(status)
	end
end

function RequestRestartArc(args)
	local job_id = args.arc_id or nil
	if (Player.GetJobStatus() ~= nil) then
		local arc_id = tostring(Player.GetJobStatus().job.arc_id)
		Debug.Log("Requesting Cancel Arc_Id: ", arc_id)
		Game.RequestCancelArc(arc_id)
		Callback2.FireAndForget(RequestRestartArc,{arc_id=arc_id}, RESTART_DELAY_SECONDS)
	end
	if (job_id ~= nil) then
		Debug.Log("Starting Arc_Id: ", job_id)
		Game.RequestStartArc(job_id)
	end
end

function ResetText()
	JPER:SetText("Percent Complete:")
	NAME:SetText("NAME:")
	JID:SetText("ID:")
	DESC:SetText("DESCRIPTION")
end

function UpdateText(status)
	--completion_percent=0,job={name="caldera commando",arc_id=394,description="admiral mokiao has a special request from uasocom to take down an elite chosen commando unit.",icon_id=271170}
	if status ~= nil then
		JPER:SetText(unicode.format("Percent Complete: %d%%", status.completion_percent*100))
		NAME:SetText("NAME: "..status.job.name)
		JID:SetText("ID: "..status.job.arc_id)
		DESC:SetText("DESCRIPTION: "..status.job.description)
	else
		Debug.Log("UpdateText status is nil, resetting")
		ResetText()
	end
end

function ShowJobManager()
	Component.SetInputMode("cursor")
	MAIN:Show(true)
end

function HideJobManager()
	MAIN:Show(false)
	Component.SetInputMode("none")
end

function GetLJobLedgerList(args)
	Debug.Table("Game.GetLJobLedgerList", Game.GetLJobLedgerList())
end